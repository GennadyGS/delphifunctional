program PrimeNumbers;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Functional.Base;

function GetPrimeNumbers: TSequence<Int64>;
begin
  Result :=
    procedure (const AProducer: TSequence<Int64>.IProducer)

      function BuildStartIsPrimePredicate(): TPredicate<Int64>;
      begin
        Result :=
          function (AValue: Int64): Boolean
          begin
            Result := True;
          end;
      end;

      function BuildNextIsPrimePredicate(const APrevIsPrimePredicate: TPredicate<Int64>;
        APrime: Int64): TPredicate<Int64>;
      begin
        Result :=
          function (AValue: Int64): Boolean
          begin
            Result := APrevIsPrimePredicate(AValue) and ((AValue mod APrime) <> 0);
          end;
      end;

    var
      IsPrimePredicate: TPredicate<Int64>;
      I: Int64;
    begin
      IsPrimePredicate := BuildStartIsPrimePredicate();
      I := 1;
      while True do
      begin
        AProducer.Yield(I);
        repeat
          Inc(I);
        until IsPrimePredicate(I);
        IsPrimePredicate := BuildNextIsPrimePredicate(IsPrimePredicate, I);
      end;
    end;
end;

var
  I: Int64;
begin
  try
    for I in GetPrimeNumbers do
      Writeln(I);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
