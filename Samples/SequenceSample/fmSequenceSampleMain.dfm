object frmSequenceSample: TfrmSequenceSample
  Left = 0
  Top = 0
  Caption = 'Sequence sample'
  ClientHeight = 322
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    502
    322)
  PixelsPerInch = 96
  TextHeight = 13
  object mmLog: TMemo
    Left = 8
    Top = 37
    Width = 486
    Height = 277
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnTest: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Action = acTest
    TabOrder = 1
  end
  object aclMain: TActionList
    Left = 16
    Top = 40
    object acTest: TAction
      Caption = 'Test'
      OnExecute = acTestExecute
    end
  end
end
