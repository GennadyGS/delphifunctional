program SequenceSample;

uses
  Vcl.Forms,
  fmSequenceSampleMain in 'fmSequenceSampleMain.pas' {frmSequenceSample};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmSequenceSample, frmSequenceSample);
  Application.Run;
end.
