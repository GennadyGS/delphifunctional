unit fmSequenceSampleMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ActnList,
  Functional.Base;

type
  TfrmSequenceSample = class(TForm)
    aclMain: TActionList;
    acTest: TAction;
    btnTest: TButton;
    mmLog: TMemo;
    procedure acTestExecute(Sender: TObject);
  strict private
    procedure Log(const AMsg: String); overload;
    procedure Log(const AMsg: String; const AArgs: array of const); overload;
  end;

var
  frmSequenceSample: TfrmSequenceSample;

implementation

{$R *.dfm}

{ TfrmSequenceSample }

procedure TfrmSequenceSample.Log(const AMsg: String);
begin
  mmLog.Lines.Add(Format('0x%x  %s', [GetCurrentThreadId, AMsg]));
end;

procedure TfrmSequenceSample.acTestExecute(Sender: TObject);

  function CreateSequence: TSequence<NativeInt>;
  begin
    Result :=
      procedure (const AProducer: TSequence<NativeInt>.IProducer)
      var
        I: NativeInt;
      begin
        Log('Sequence: Enter');
        try
          I := 0;
          while True do
          begin
            Log('Sequence: Before Yield(%d)', [I]);
            AProducer.Yield(I);
            Log('Sequence: After Yield(%d)', [I]);
            Inc(I);
          end;
        finally
          Log('Sequence: Finally');
        end;
        Log('Sequence: Exit');
      end;
  end;

var
  Seq: TSequence<NativeInt>;
  I: NativeInt;
begin
  Log('Client: Before CreateSequence');
  Seq := CreateSequence();
  Log('Client: After CreateSequence');
  Log('Client: Before enumerating sequence');
  for I in Seq do
  begin
    Log('Client: Enumerating next item: %d', [I]);
    if I >= 10 then
    begin
      Log('Client: Break enumerating');
      Break;
    end;
  end;
  Log('Client: After enumerating sequence')
end;

procedure TfrmSequenceSample.Log(const AMsg: String;
  const AArgs: array of const);
begin
  Log(Format(AMsg, AArgs));
end;

end.
