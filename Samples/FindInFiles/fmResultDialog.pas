unit fmResultDialog;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfrmResultDialog = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    edLineResult: TEdit;
    lbFileName: TLabel;
    lbFileNameResult: TLabel;
    lbPosition: TLabel;
    lbPositionResult: TLabel;
    lbResult: TLabel;
    procedure FormShow(Sender: TObject);
  strict private
    FPos: NativeInt;
    FSubstrLen: NativeInt;
  public
    class function ShowResult(const AFileName: string; ALineNumber, APos,
      ASubstrLen: NativeInt; const ALine: string): Boolean;
  end;

implementation

{$R *.dfm}

{ TfrmResultDialog }

procedure TfrmResultDialog.FormShow(Sender: TObject);
begin
  edLineResult.SetFocus;
  edLineResult.SelStart := FPos - 1;
  edLineResult.SelLength := FSubstrLen;
end;

class function TfrmResultDialog.ShowResult(const AFileName: string; ALineNumber,
  APos, ASubstrLen: NativeInt; const ALine: string): Boolean;
var
  LDialog: TfrmResultDialog;
begin
  LDialog := TfrmResultDialog.Create(nil);
  try
    LDialog.lbFileNameResult.Caption := AFileName;
    LDialog.lbPositionResult.Caption := Format('%d:%d', [ALineNumber, APos]);
    LDialog.edLineResult.Text := ALine;
    LDialog.FPos := APos;
    LDialog.FSubStrLen := ASubstrLen;
    Result := LDialog.ShowModal = mrOk;
  finally
    LDialog.Free;
  end;
end;

end.
