unit fmFindInFilesMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ActnList,
  Vcl.Buttons, Vcl.ComCtrls,
  Functional.Base,
  uFindInFiles, fmResultDialog;

type
  TfrmFindInFilesMain = class(TForm)
    acSearch: TAction;
    aclMain: TActionList;
    acSelectPath: TAction;
    acStop: TAction;
    btnFindFiles: TButton;
    btnSelectPath: TSpeedButton;
    btnStop: TButton;
    cbRecursive: TCheckBox;
    dlgSelectPath: TOpenDialog;
    edMask: TEdit;
    edPath: TEdit;
    edSearchText: TEdit;
    lbMask: TLabel;
    lbPath: TLabel;
    lbSearchText: TLabel;
    mmResults: TMemo;
    stbMain: TStatusBar;
    procedure acSearchExecute(Sender: TObject);
    procedure aclMainUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure acSelectPathExecute(Sender: TObject);
    procedure acStopExecute(Sender: TObject);
  strict private
    FActive: Boolean;
    FStopping: Boolean;
    function CheckContinue: Boolean;
    procedure UpdateActionStates;
    procedure SearchTextInFiles(const ASearchText, APath, AMask: String;
      ARecursive: Boolean);
    procedure ShowCurrentDir(const ADir: String);
    procedure StartSearch(const APath: String);
    procedure StopSearch;
    procedure TerminateSearch;
  end;

var
  frmFindInFilesMain: TfrmFindInFilesMain;

implementation

{$R *.dfm}

procedure TfrmFindInFilesMain.acSearchExecute(Sender: TObject);
begin
  SearchTextInFiles(edSearchText.Text, edPath.Text, edMask.Text, cbRecursive.Checked);
end;

procedure TfrmFindInFilesMain.aclMainUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  UpdateActionStates;
end;

procedure TfrmFindInFilesMain.acSelectPathExecute(Sender: TObject);
begin
  if dlgSelectPath.Execute then
    edPath.Text := dlgSelectPath.FileName;
end;

procedure TfrmFindInFilesMain.acStopExecute(Sender: TObject);
begin
  FStopping := True;
end;

function TfrmFindInFilesMain.CheckContinue: Boolean;
begin
  UpdateActionStates;
  Application.ProcessMessages;
  Result := not FStopping;
end;

procedure TfrmFindInFilesMain.SearchTextInFiles(const ASearchText, APath,
  AMask: String; ARecursive: Boolean);

  procedure SearchTextInFile(const ASearchText, AFileName: String);
  var
    LLineNumber, LPosition, LSearchTextLen: NativeInt;
    LLine: String;
  begin
    LSearchTextLen := Length(ASearchText);
    LLineNumber := 1;
    for LLine in TFindInFilesUtils.EnumLines(AFileName) do
    begin
      for LPosition in TFindInFilesUtils.SearchAllSubstrs(ASearchText, LLine, True) do
      begin
        mmResults.Lines.Add(Format('%s (%d:%d): %s',
          [AFileName, LLineNumber, LPosition, LLine]));
        if not TfrmResultDialog.ShowResult(AFileName, LLineNumber, LPosition,
          LSearchTextLen, LLine) then
            StopSearch;
        if not CheckContinue then
          Exit;
      end;
      Inc(LLineNumber);
    end;
  end;

var
  LFileName: String;
begin
  StartSearch(APath);
  try
    for LFileName in TFindInFilesUtils.FindFiles(APath, AMask, ARecursive,
      function (const ADirName: String): Boolean
      begin
        ShowCurrentDir(ADirName);
        Result := CheckContinue;
      end
    ) do
    begin
      SearchTextInFile(ASearchText, LFileName);
      if not CheckContinue then
        Break;
    end;
  finally
    TerminateSearch;
  end;
end;

procedure TfrmFindInFilesMain.ShowCurrentDir(const ADir: String);
begin
  stbMain.Panels[0].Text := ADir;
end;

procedure TfrmFindInFilesMain.StartSearch(const APath: String);
begin
  mmResults.Clear;
  ShowCurrentDir(APath);
  FStopping := False;
  FActive := True;
end;

procedure TfrmFindInFilesMain.StopSearch;
begin
  FStopping := True;
end;

procedure TfrmFindInFilesMain.TerminateSearch;
begin
  try
    if not FStopping then
      ShowMessage('Search complete');
    ShowCurrentDir('');
  finally
    FActive := False;
    FStopping := False;
  end;
end;

procedure TfrmFindInFilesMain.UpdateActionStates;
begin
  acSearch.Enabled := not FActive;
  acStop.Enabled := FActive and not FStopping;
end;

end.

