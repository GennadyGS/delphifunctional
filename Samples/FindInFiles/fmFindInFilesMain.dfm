object frmFindInFilesMain: TfrmFindInFilesMain
  Left = 0
  Top = 0
  Caption = 'Find in files'
  ClientHeight = 318
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbPath: TLabel
    Left = 8
    Top = 16
    Width = 22
    Height = 13
    Caption = 'Path'
  end
  object lbMask: TLabel
    Left = 8
    Top = 43
    Width = 24
    Height = 13
    Caption = 'Mask'
  end
  object btnSelectPath: TSpeedButton
    Left = 415
    Top = 8
    Width = 23
    Height = 22
    Action = acSelectPath
    Caption = '...'
  end
  object lbSearchText: TLabel
    Left = 8
    Top = 70
    Width = 45
    Height = 13
    Caption = 'With text'
  end
  object btnFindFiles: TButton
    Left = 8
    Top = 99
    Width = 65
    Height = 24
    Action = acSearch
    Default = True
    TabOrder = 0
  end
  object edPath: TEdit
    Left = 59
    Top = 8
    Width = 350
    Height = 21
    TabOrder = 1
    Text = 'd:\Source\Delphi\Functional\Source'
  end
  object mmResults: TMemo
    Left = 0
    Top = 129
    Width = 445
    Height = 170
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object edMask: TEdit
    Left = 59
    Top = 35
    Width = 166
    Height = 21
    TabOrder = 3
    Text = '*.pas'
  end
  object cbRecursive: TCheckBox
    Left = 248
    Top = 37
    Width = 73
    Height = 17
    Caption = 'Recursive'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object edSearchText: TEdit
    Left = 59
    Top = 62
    Width = 166
    Height = 21
    TabOrder = 5
  end
  object stbMain: TStatusBar
    Left = 0
    Top = 299
    Width = 445
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object btnStop: TButton
    Left = 88
    Top = 99
    Width = 65
    Height = 24
    Action = acStop
    TabOrder = 7
  end
  object aclMain: TActionList
    OnUpdate = aclMainUpdate
    Left = 408
    Top = 264
    object acSearch: TAction
      Caption = 'Search'
      OnExecute = acSearchExecute
    end
    object acSelectPath: TAction
      Caption = 'acSelectPath'
      OnExecute = acSelectPathExecute
    end
    object acStop: TAction
      Caption = 'Stop'
      OnExecute = acStopExecute
    end
  end
  object dlgSelectPath: TOpenDialog
    Options = [ofHideReadOnly, ofNoLongNames, ofEnableSizing]
    Left = 368
    Top = 264
  end
end
