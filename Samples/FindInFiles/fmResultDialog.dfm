object frmResultDialog: TfrmResultDialog
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Text found'
  ClientHeight = 135
  ClientWidth = 519
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbFileName: TLabel
    Left = 16
    Top = 8
    Width = 43
    Height = 13
    Caption = 'FileName'
  end
  object lbPosition: TLabel
    Left = 16
    Top = 37
    Width = 37
    Height = 13
    Caption = 'Position'
  end
  object lbResult: TLabel
    Left = 16
    Top = 61
    Width = 19
    Height = 13
    Caption = 'Line'
  end
  object lbPositionResult: TLabel
    Left = 88
    Top = 39
    Width = 75
    Height = 13
    Caption = 'lbPositionResult'
  end
  object lbFileNameResult: TLabel
    Left = 88
    Top = 8
    Width = 81
    Height = 13
    Caption = 'lbFileNameResult'
  end
  object btnOK: TButton
    Left = 16
    Top = 94
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 108
    Top = 94
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object edLineResult: TEdit
    Left = 88
    Top = 58
    Width = 423
    Height = 21
    ReadOnly = True
    TabOrder = 2
  end
end
