unit uFindInFiles;

interface

uses
  SysUtils, Classes, IOUtils, StrUtils,
  Functional.Base;

const
  SAllFilesMask = '*';
  SCurrentDir: string = '.';
  SParentDir: string = '..';

type
  TChangeDirEvent = reference to function (const ADirPath: string): Boolean;

  TFindInFilesUtils = class sealed
  strict private
    class function GetSearchRecToFileNameConvertor(const APath: string): TFunc<TSearchRec, string>;
    class function GetFilesInfo(const APath: string;
      AAtributes: Integer = faAnyFile): TSequence<TSearchRec>;
    class function FindFilesInDir(const APath: string;
      const AFileMask: string): TSequence<string>;
    class function FindSubDirsInDir(const APath: string): TSequence<string>;
    class function InternalFindFiles(const APath: string; const AFileMask: string;
      ARecursive: Boolean; const AChangeDirEvent: TChangeDirEvent): TSequence<string>;
    class function ReadLineFromStream(AStream: TStream): AnsiString;
  public
    class function FindFiles(const APath: string; const AFileMask: string = '';
      ARecursive: Boolean = True; const AChangeDirEvent: TChangeDirEvent = nil): TSequence<string>;
    class function EnumLines(const AFileName: string): TSequence<string>;
    class function SearchAllSubstrs(const ASubstr, AStr: string;
      ACaseSensitive: Boolean): TSequence<NativeInt>;
  end;

implementation

{ TFindInFilesUtils }

class function TFindInFilesUtils.EnumLines(
  const AFileName: string): TSequence<string>;
var
  LStream: TStream;
begin
  Result :=
    procedure (const AProducer: TSequence<string>.IProducer)
    begin
      LStream := TFileStream.Create(AFileName, fmOpenRead);
      try
        while LStream.Position < LStream.Size do
          AProducer.Yield(string(ReadLineFromStream(LStream)));
      finally
        LStream.Free;
      end;
    end;
end;

class function TFindInFilesUtils.FindFiles(const APath, AFileMask: string;
  ARecursive: Boolean; const AChangeDirEvent: TChangeDirEvent): TSequence<string>;

  function GetFileMaskOrDefault: string;
  begin
    if AFileMask = '' then
      Exit(SAllFilesMask);
    Result := AFileMask;
  end;

begin
  Result := InternalFindFiles(APath, GetFileMaskOrDefault, ARecursive, AChangeDirEvent);
end;

class function TFindInFilesUtils.FindFilesInDir(const APath,
  AFileMask: string): TSequence<string>;
var
  LFilesInfo: TSequence<TSearchRec>;
begin
  LFilesInfo := GetFilesInfo(TPath.Combine(APath, AFileMask)) >=
    function (AItem: TSearchRec): Boolean
    begin
      Result := AItem.Attr and faDirectory = 0;
    end;
  Result := LFilesInfo.Map<string>(GetSearchRecToFileNameConvertor(APath));
end;

class function TFindInFilesUtils.FindSubDirsInDir(
  const APath: string): TSequence<string>;
var
  LFilesInfo: TSequence<TSearchRec>;
begin
  LFilesInfo := GetFilesInfo(TPath.Combine(APath, SAllFilesMask)) >=
    function (AItem: TSearchRec): Boolean
    begin
      Result := (AItem.Attr and faDirectory = faDirectory)
        and (AItem.Name <> SCurrentDir)
        and (AItem.Name <> SParentDir)
    end;
  Result := LFilesInfo.Map<string>(GetSearchRecToFileNameConvertor(APath));
end;

class function TFindInFilesUtils.GetFilesInfo(const APath: string;
  AAtributes: Integer = faAnyFile): TSequence<TSearchRec>;
var
  SearchRec: TSearchRec;
begin
  Result :=
    procedure (const AProducer: TSequence<TSearchRec>.IProducer)
    begin
      if FindFirst(APath, AAtributes, SearchRec) = 0 then
        try
          repeat
            AProducer.Yield(SearchRec);
          until FindNext(SearchRec) <> 0;
        finally
          FindClose(SearchRec);
        end;
    end;
end;

class function TFindInFilesUtils.GetSearchRecToFileNameConvertor(
  const APath: string): TFunc<TSearchRec, string>;
begin
  Result :=
    function (ASearchRec: TSearchRec): string
    begin
      Result := TPath.Combine(APath, ASearchRec.Name);
    end;
end;

class function TFindInFilesUtils.InternalFindFiles(const APath,
  AFileMask: string; ARecursive: Boolean; const AChangeDirEvent: TChangeDirEvent): TSequence<string>;
begin
  Result :=
    procedure (const AProducer: TSequence<string>.IProducer)
    var
      LSubDir: string;
    begin
      AProducer.YieldRecursive(FindFilesInDir(APath, AFileMask));
      if ARecursive then
        for LSubDir in FindSubDirsInDir(APath) do
        begin
          if Assigned(AChangeDirEvent) and not AChangeDirEvent(LSubDir) then
            Break;
          AProducer.YieldRecursive(InternalFindFiles(LSubDir, AFileMask, True, AChangeDirEvent));
         end;
    end;
end;

class function TFindInFilesUtils.ReadLineFromStream(
  AStream: TStream): AnsiString;
const
  SCRChar = #13;
  SLFChar = #10;
  SLineBreakChars = [SCRChar, SLFChar];
var
  LChar, LFirstBreakChar: AnsiChar;
begin
  LFirstBreakChar := #0;
  Result := '';
  while AStream.Read(LChar, SizeOf(LChar)) > 0 do
  begin
    if LChar in SLineBreakChars then
    begin
      LFirstBreakChar := LChar;
      Break;
    end;
    Result := Result + LChar;
  end;
  if LFirstBreakChar = SCRChar then
  begin
    AStream.Read(LChar, SizeOf(LChar));
    if LChar <> SLFChar then
      AStream.Seek(-SizeOf(LChar), soFromCurrent);
  end;
end;

class function TFindInFilesUtils.SearchAllSubstrs(const ASubstr, AStr: string;
  ACaseSensitive: Boolean): TSequence<NativeInt>;
begin
  Result :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    var
      LPosition: NativeInt;
    begin
      LPosition := 1;
      repeat
        LPosition := PosEx(ASubstr, AStr, LPosition + 1);
        if LPosition > 0 then
          AProducer.Yield(LPosition);
      until LPosition = 0;
    end;
end;

end.
