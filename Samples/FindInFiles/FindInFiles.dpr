program FindInFiles;

uses
  Vcl.Forms,
  fmFindInFilesMain in 'fmFindInFilesMain.pas' {frmFindInFilesMain},
  uFindInFiles in 'uFindInFiles.pas',
  fmResultDialog in 'fmResultDialog.pas' {frmResultDialog};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmFindInFilesMain, frmFindInFilesMain);
  Application.Run;
end.
