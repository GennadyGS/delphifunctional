program Fibonacci;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Functional.Base;

function GetFibonacci: TSequence<Int64>;
begin
  Result :=
    procedure (const AProducer: TSequence<Int64>.IProducer)
    var
      Current, Prev1, Prev2: Int64;
    begin
      Prev1 := 0;
      Prev2 := 1;
      while True do
      begin
        Current := Prev1 + Prev2;
        Prev2 := Prev1;
        Prev1 := Current;
        AProducer.Yield(Current);
      end;
    end
end;

var
  I: Int64;
begin
  try
    for I in GetFibonacci do
      Writeln(I);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
