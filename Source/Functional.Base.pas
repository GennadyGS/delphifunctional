unit Functional.Base;

interface

uses
  Winapi.Windows, System.SysUtils, Generics.Collections, Generics.Defaults,
  Functional.Utils, Functional.Winapi.Fibers, Functional.Fibers;

type
  ERecList = class(Exception);

  TRecList<T> = record
  strict private
    type
      TData = record
      strict private
        FHead: T;
        FTail: IInterface;
      public
        constructor Create(AHead: T; const ATail: IInterface);
        property Head: T read FHead;
        property Tail: IInterface read FTail;
      end;

      TReduceFunc = reference to function (const AAccum: T; const AItem: T): T;

      TFoldFunc<TResult> = reference to function (const AAccum: TResult;
        const AItem: T): TResult;
    var
      FData: IWrapper<TData>;

    class function CompareWithPointer(const AComparand1: TRecList<T>;
      const AComparand2: Pointer): Boolean; overload; static;

    function GetEmpty: Boolean;
    function GetHead: T;
    function GetSize: NativeInt;
    function GetTail: TRecList<T>;
    function InternalReverse(const APrevResult: TRecList<T>): TRecList<T>;
    procedure VerifyNotEmpty;
  public
    constructor Create(const AHead: T); overload;
    constructor Create(const AHead: T; const ATail: TRecList<T>); overload;
    property Head: T read GetHead;
    property Tail: TRecList<T> read GetTail;
    property Empty: Boolean read GetEmpty;
    property Size: NativeInt read GetSize;

    class function CreateEmpty: TRecList<T>; static;
    class function Concatenate(const AList1, AList2: TRecList<T>): TRecList<T>; static;
    class function Compare(const AComparand1, AComparand2: TRecList<T>;
      const AComparer: IEqualityComparer<T>): Boolean; overload; static;
    class function Compare(const AComparand1, AComparand2: TRecList<T>): Boolean; overload; static; 
    class function CompareWithArray(const AComparand1: TRecList<T>;
      const AComparand2: TArray<T>; const AComparer: IEqualityComparer<T>;
      AArrayStartIndex: NativeInt = 0): Boolean; overload; static;
    class function CompareWithArray(const AComparand1: TRecList<T>;
      const AComparand2: TArray<T>; AArrayStartIndex: NativeInt = 0): Boolean; overload; static;
    class function FromArray(const AValue: TArray<T>; AArrayStartIndex: NativeInt = 0): TRecList<T>; static;

    class operator Implicit(const AValue: T): TRecList<T>;
    class operator Implicit(const AValue: TArray<T>): TRecList<T>;
    class operator Implicit(AValue: Pointer): TRecList<T>;
    class operator Equal(const AComparand1: TRecList<T>; AComparand2: Pointer): Boolean;
    class operator Equal(const AComparand1, AComparand2: TRecList<T>): Boolean;
    class operator Equal(const AComparand1: TRecList<T>; const AComparand2: TArray<T>): Boolean;
    class operator NotEqual(const AComparand1: TRecList<T>; AComparand2: Pointer): Boolean;
    class operator NotEqual(const AComparand1, AComparand2: TRecList<T>): Boolean;
    class operator NotEqual(const AComparand1: TRecList<T>; const AComparand2: TArray<T>): Boolean;
    class operator Add(const AList1, AList2: TRecList<T>): TRecList<T>;
    class operator Multiply(const AHead: T; const ATail: TRecList<T>): TRecList<T>;
    class operator GreaterThan(const AList: TRecList<T>;
      const AReduceFunc: TReduceFunc): T;
    class operator LessThan(const AList: TRecList<T>;
      const AMapFunc: TFunc<T, T>): TRecList<T>;
    class operator LessThan(const AList: TRecList<T>;
      const AProc: TProc<T>): TRecList<T>;

    function Map<TResult>(const AMapFunc: TFunc<T, TResult>): TRecList<TResult>; overload;
    function Map(const AMapFunc: TFunc<T, T>): TRecList<T>; overload;
    function Fold<TResult>(const AFoldFunc: TFoldFunc<TResult>;
      const AAccum: TResult): TResult;
    procedure ForAll(const AProc: TProc<T>);
    function Reduce(const AReduceFunc: TReduceFunc): T; overload;
    function Reduce(const AReduceFunc: TReduceFunc; const AAccum: T): T; overload;
    function Reverse: TRecList<T>;
  end;

  TSequence<T> = record
  public
    type
      IProducer = interface
        procedure Yield(const AItem: T);
        procedure YieldRecursive(const AItems: TSequence<T>);
      end;

      TGeneratorProc = reference to procedure (const AProducer: IProducer);

      TReduceFunc = reference to function (const AAccum: T; const AItem: T): T;

      TFoldFunc<TResult> = reference to function (const AAccum: TResult;
        const AItem: T): TResult;

      TUnfoldFuncResult<TState> = record
      strict private
        FComplete: Boolean;
        FItem: T;
        FNewState: TState;
      public  
        class function CreateItem(const AItem: T; const ANewState: TState): TUnfoldFuncResult<TState>; static;
        class function CreteComplete: TUnfoldFuncResult<TState>; static;
        property Complete: Boolean read FComplete;
        property Item: T read FItem;
        property NewState: TState read FNewState;
      end;

      TUnfoldFunc<TState> = reference to function (const AState: TState): TUnfoldFuncResult<TState>;
  strict private
    type
      TGenaratedEnumerator = class(TInterfacedObject, IEnumerator<T>, IProducer)
      strict private
        FCallingThreadFiber: TFiber;
        FDestroying: Boolean;
        FLastItem: T;
        FFiber: TFiber;
        FGeneratorProc: TGeneratorProc;
        FGeneratorProcExceptAddr: Pointer;
        FGeneratorProcExceptObject: TObject;
        FGeneratorProcTerminated: Boolean;
        FGeneratorProcStarted: Boolean;
        procedure FiberProc();
        procedure ReraiseGeneratorProcException();
        procedure SwitchToCallingFiber();
      strict protected
        { IEnumerator<T> }
        function GetCurrent: T;
        function MoveNext: Boolean;
        { IProducer }
        procedure Yield(const AItem: T);
        procedure YieldRecursive(const AItems: TSequence<T>);
      public
        constructor Create(const AGeneratorProc: TGeneratorProc);
        destructor Destroy; override;
        procedure BeforeDestruction; override;
      end;
    var
      FEnumerable: IEnumerable<T>;

    class function CompareWithPointer(const AComparand1: TSequence<T>;
      const AComparand2: Pointer): Boolean; overload; static;

    function GetEmpty: Boolean;
    function GetSize: NativeInt;
  public
    constructor Create(const AGeneratorProc: TGeneratorProc); overload;
    constructor Create(const AEnumerable: IEnumerable<T>); overload;
    function GetEnumerator: IEnumerator<T>;
    property Empty: Boolean read GetEmpty;
    property Size: NativeInt read GetSize;

    class function CreateEmpty: TSequence<T>; static;
    class function Compare(const AComparand1, AComparand2: TSequence<T>;
      const AComparer: IEqualityComparer<T>): Boolean; overload; static;
    class function Compare(const AComparand1, AComparand2: TSequence<T>): Boolean; overload; static;
    class function CompareWithArray(const AComparand1: TSequence<T>;
      const AComparand2: TArray<T>; const AComparer: IEqualityComparer<T>;
      AArrayStartIndex: NativeInt = 0): Boolean; overload; static;
    class function CompareWithArray(const AComparand1: TSequence<T>;
      const AComparand2: TArray<T>; AArrayStartIndex: NativeInt = 0): Boolean; overload; static;
    class function Concatenate(const ASequence1, ASequence2: TSequence<T>): TSequence<T>; static;
    class function FromArray(const AValue: TArray<T>): TSequence<T>; static;
    class function FromRecList(const AValue: TRecList<T>): TSequence<T>; static;
    class function FromSingleItem(const AValue: T): TSequence<T>; static;
    class function InitInfinite(const AGeneratorFunc: TFunc<NativeInt, T>): TSequence<T>; static;
    class function Unfold<TState>(const AUnfoldFunc: TUnfoldFunc<TState>; 
      const AInitialState: TState): TSequence<T>; static;

    class operator Implicit(const AValue: IEnumerable<T>): TSequence<T>;
    class operator Implicit(const AValue: TGeneratorProc): TSequence<T>;
    class operator Implicit(const AValue: TArray<T>): TSequence<T>;
    class operator Implicit(const AValue: T): TSequence<T>;
    class operator Implicit(const AValue: TRecList<T>): TSequence<T>;
    class operator Implicit(AValue: Pointer): TSequence<T>;
    class operator Equal(const AComparand1: TSequence<T>; AComparand2: Pointer): Boolean;
    class operator Equal(const AComparand1, AComparand2: TSequence<T>): Boolean;
    class operator Equal(const AComparand1: TSequence<T>; const AComparand2: TArray<T>): Boolean;
    class operator NotEqual(const AComparand1: TSequence<T>; AComparand2: Pointer): Boolean;
    class operator NotEqual(const AComparand1, AComparand2: TSequence<T>): Boolean;
    class operator NotEqual(const AComparand1: TSequence<T>; const AComparand2: TArray<T>): Boolean;
    // Concatenate
    class operator Add(const ASequence1, ASequence2: TSequence<T>): TSequence<T>;
    // Reduce
    class operator GreaterThan(const ASequence: TSequence<T>;
      const AReduceFunc: TReduceFunc): T;
    // Filter
    class operator GreaterThanOrEqual(const ASequence: TSequence<T>;
      const APredicate: TPredicate<T>): TSequence<T>;
    // Map
    class operator LessThan(const ASequence: TSequence<T>;
      const AMapFunc: TFunc<T, T>): TSequence<T>;
    class operator LessThan(const ASequence: TSequence<T>;
      const AProc: TProc<T>): TSequence<T>;

    function Map<TResult>(const AMapFunc: TFunc<T, TResult>): TSequence<TResult>; overload;
    function Map(const AMapFunc: TFunc<T, T>): TSequence<T>; overload;
    function Filter(const APredicate: TPredicate<T>): TSequence<T>;
    function Fold<TResult>(const AFoldFunc: TFoldFunc<TResult>; const AAccum: TResult): TResult;
    procedure ForAll(const AProc: TProc<T>);
    function Skip(ASkippedCount: NativeInt): TSequence<T>;
    function Reduce(const AReduceFunc: TReduceFunc): T; overload;
    function Reduce(const AReduceFunc: TReduceFunc; const AAccum: T): T; overload;
  end;

resourcestring
  SErrCannotAssignNotNullValueToRecList = 'Cannot assign not null pointer to RecList';
  SErrCannotAssignNotNullValueToSequence = 'Cannot assign not null pointer to Sequence';
  SErrCannotCompareNotNullValueWithRecList = 'Cannot conmpare not null pointer with RecList';
  SErrCannotCompareNotNullValueWithSequence = 'Cannot conmpare not null pointer with Sequence';
  SErrReduceEmptyList = 'Empty list cannot be reduced';
  SErrReduceEmptySequence = 'Empty sequence cannot be reduced';

implementation

{ TRecList<T> }

constructor TRecList<T>.Create(const AHead: T);
begin
  Create(AHead, nil);
end;

class operator TRecList<T>.Add(const AList1, AList2: TRecList<T>): TRecList<T>;
begin
  Result := Concatenate(AList1, AList2);
end;

class operator TRecList<T>.Equal(const AComparand1: TRecList<T>; AComparand2: Pointer): Boolean;
begin
  Result := CompareWithPointer(AComparand1, AComparand2);
end;

class function TRecList<T>.Compare(const AComparand1,
  AComparand2: TRecList<T>; const AComparer: IEqualityComparer<T>): Boolean;
var
  LComparer: IEqualityComparer<T>;
begin
  if AComparand1.Empty or AComparand2.Empty then
    Exit(AComparand1.Empty = AComparand2.Empty);
  LComparer := TFunctionalUtils.GetEqualityComparerOrDefault<T>(AComparer);
  Result := LComparer.Equals(AComparand1.Head, AComparand2.Head) 
    and (AComparand1.Tail = AComparand2.Tail);
end;

class function TRecList<T>.Compare(const AComparand1,
  AComparand2: TRecList<T>): Boolean;
begin
  Result := Compare(AComparand1, AComparand2, nil)
end;

class function TRecList<T>.CompareWithArray(const AComparand1: TRecList<T>;
  const AComparand2: TArray<T>; const AComparer: IEqualityComparer<T>;
  AArrayStartIndex: NativeInt): Boolean;
var
  LArrayEmpty: Boolean;
  LComparer: IEqualityComparer<T>;
begin
  LArrayEmpty := AArrayStartIndex >= Length(AComparand2);
  if LArrayEmpty or AComparand1.Empty then
    Exit(LArrayEmpty = AComparand1.Empty);
  LComparer := TFunctionalUtils.GetEqualityComparerOrDefault<T>(AComparer);
  Result := LComparer.Equals(AComparand1.Head, AComparand2[AArrayStartIndex])
    and CompareWithArray(AComparand1.Tail, AComparand2, AArrayStartIndex + 1);
end;

class function TRecList<T>.CompareWithArray(const AComparand1: TRecList<T>;
  const AComparand2: TArray<T>; AArrayStartIndex: NativeInt): Boolean;
begin
  Result := CompareWithArray(AComparand1, AComparand2, nil, AArrayStartIndex);
end;

class function TRecList<T>.CompareWithPointer(const AComparand1: TRecList<T>;
  const AComparand2: Pointer): Boolean;
begin
  if AComparand2 <> nil then
    raise EInvalidOp.Create(SErrCannotCompareNotNullValueWithRecList);
  Result := AComparand1.Empty;
end;

class function TRecList<T>.Concatenate(const AList1,
  AList2: TRecList<T>): TRecList<T>;
begin
  if AList1.Empty then
    Exit(AList2);
  Result := TRecList<T>.Create(AList1.Head,
    Concatenate(AList1.Tail, AList2));
end;

constructor TRecList<T>.Create(const AHead: T; const ATail: TRecList<T>);
begin
  FData := TWrapper<TData>.Create(
    TData.Create(AHead, IInterface(ATail)));
end;

class function TRecList<T>.CreateEmpty: TRecList<T>;
begin
  FillChar(Result, SizeOf(Result), 0);
end;

class operator TRecList<T>.Equal(const AComparand1,
  AComparand2: TRecList<T>): Boolean;
begin
  Result := Compare(AComparand1, AComparand2);
end;

function TRecList<T>.Fold<TResult>(const AFoldFunc: TFoldFunc<TResult>;
  const AAccum: TResult): TResult;
begin
  if Empty then
    Exit(AAccum);
  Result := Tail.Fold<TResult>(AFoldFunc, AFoldFunc(AAccum, Head));
end;

procedure TRecList<T>.ForAll(const AProc: TProc<T>);
begin
  if Empty then
    Exit;
  AProc(Head);
  Tail.ForAll(AProc);
end;

class function TRecList<T>.FromArray(const AValue: TArray<T>;
  AArrayStartIndex: NativeInt): TRecList<T>;
begin
  if AArrayStartIndex >= Length(AValue) then
    Exit(nil);
  Result := AValue[AArrayStartIndex] * FromArray(AValue, AArrayStartIndex + 1);
end;

function TRecList<T>.GetEmpty: Boolean;
begin
  Result := not Assigned(FData);
end;

function TRecList<T>.GetHead: T;
begin
  VerifyNotEmpty;
  Result := FData.Value.Head;
end;

function TRecList<T>.GetSize: NativeInt;
begin
  if Empty then
    Exit(0);
   Result := Tail.Size + 1;
end;

function TRecList<T>.GetTail: TRecList<T>;
begin
  VerifyNotEmpty;
  Result := TRecList<T>(FData.Value.Tail);
end;

class operator TRecList<T>.GreaterThan(const AList: TRecList<T>;
  const AReduceFunc: TReduceFunc): T;
begin
  Result := AList.Reduce(AReduceFunc);
end;

class operator TRecList<T>.Implicit(AValue: Pointer): TRecList<T>;
begin
  if AValue <> nil then
    raise EInvalidOpException.Create(SErrCannotAssignNotNullValueToRecList);
  Result := CreateEmpty;
end;

class operator TRecList<T>.Implicit(const AValue: TArray<T>): TRecList<T>;
begin
  Result := FromArray(AValue);
end;

function TRecList<T>.InternalReverse(const APrevResult: TRecList<T>): TRecList<T>;
begin
  if Empty then
    Exit(APrevResult);
  Result := Tail.InternalReverse(Head * APrevResult);
end;

class operator TRecList<T>.LessThan(const AList: TRecList<T>;
  const AMapFunc: TFunc<T, T>): TRecList<T>;
begin
  Result := AList.Map(AMapFunc);
end;

class operator TRecList<T>.LessThan(const AList: TRecList<T>;
  const AProc: TProc<T>): TRecList<T>;
begin
  AList.ForAll(AProc);
  Result := AList;
end;

class operator TRecList<T>.Implicit(const AValue: T): TRecList<T>;
begin
  Result := TRecList<T>.Create(AValue);
end;

function TRecList<T>.Map(const AMapFunc: TFunc<T, T>): TRecList<T>;
begin
  Result := Map<T>(AMapFunc);
end;

function TRecList<T>.Map<TResult>(
  const AMapFunc: TFunc<T, TResult>): TRecList<TResult>;
begin
  if Empty then
    Exit(nil);
  Result := AMapFunc(Head) * Tail.Map<TResult>(AMapFunc);
end;

class operator TRecList<T>.Multiply(const AHead: T;
  const ATail: TRecList<T>): TRecList<T>;
begin
  Result := TRecList<T>.Create(AHead, ATail);
end;

class operator TRecList<T>.NotEqual(const AComparand1,
  AComparand2: TRecList<T>): Boolean;
begin
  Result := not Compare(AComparand1, AComparand2);
end;

class operator TRecList<T>.NotEqual(const AComparand1: TRecList<T>;
  AComparand2: Pointer): Boolean;
begin
  Result := not CompareWithPointer(AComparand1, AComparand2);
end;

class operator TRecList<T>.NotEqual(const AComparand1: TRecList<T>;
  const AComparand2: TArray<T>): Boolean;
begin
  Result := not CompareWithArray(AComparand1, AComparand2, 0);
end;

function TRecList<T>.Reduce(const AReduceFunc: TReduceFunc): T;
begin
  if Empty then
    raise EInvalidOpException.Create(SErrReduceEmptyList);
  Result := Tail.Reduce(AReduceFunc, Head);
end;

function TRecList<T>.Reduce(const AReduceFunc: TReduceFunc;
  const AAccum: T): T;
begin
  if Empty then
    Exit(AAccum);
  Result := Tail.Reduce(AReduceFunc, AReduceFunc(AAccum, Head));
end;

function TRecList<T>.Reverse: TRecList<T>;
begin
  Result := InternalReverse(nil);
end;

procedure TRecList<T>.VerifyNotEmpty;
begin
  if Empty then
    raise ERecList.Create('Recursive list is empty');
end;

class operator TRecList<T>.Equal(const AComparand1: TRecList<T>;
  const AComparand2: TArray<T>): Boolean;
begin
  Result := CompareWithArray(AComparand1, AComparand2, 0);
end;

{ TRecList<T>.TData }

constructor TRecList<T>.TData.Create(AHead: T; const ATail: IInterface);
begin
  FHead := AHead;
  FTail := ATail;
end;

{ TSequence<T> }

constructor TSequence<T>.Create(const AGeneratorProc: TGeneratorProc);
begin
  FEnumerable := TFunctionalUtils.CreateEnumeratorFactory<T>(
    function (): IEnumerator<T>
    begin
      Result := TGenaratedEnumerator.Create(AGeneratorProc);
    end
  );
end;

class operator TSequence<T>.Add(const ASequence1,
  ASequence2: TSequence<T>): TSequence<T>;
begin
  Result := Concatenate(ASequence1, ASequence2);
end;

class function TSequence<T>.Compare(const AComparand1,
  AComparand2: TSequence<T>; const AComparer: IEqualityComparer<T>): Boolean;
var
  LComparer: IEqualityComparer<T>;
  LEnumerator1, LEnumerator2: IEnumerator<T>;
  LMoveNextResult1, LMoveNextResult2: Boolean;
begin
  LComparer := TFunctionalUtils.GetEqualityComparerOrDefault<T>(AComparer);
  LEnumerator1 := AComparand1.GetEnumerator;
  LEnumerator2 := AComparand2.GetEnumerator;
  while True do
  begin
    LMoveNextResult1 := LEnumerator1.MoveNext;
    LMoveNextResult2 := LEnumerator2.MoveNext;
    if not LMoveNextResult1 and not LMoveNextResult2 then
      Exit(True);
    if (LMoveNextResult1 <> LMoveNextResult2) or 
      not LComparer.Equals(LEnumerator1.Current, LEnumerator1.Current) then
        Exit(False);
  end;
end;

class function TSequence<T>.Compare(const AComparand1,
  AComparand2: TSequence<T>): Boolean;
begin
  Result := Compare(AComparand1, AComparand2, nil);
end;

class function TSequence<T>.CompareWithArray(const AComparand1: TSequence<T>;
  const AComparand2: TArray<T>; AArrayStartIndex: NativeInt): Boolean;
begin
  Result := CompareWithArray(AComparand1, AComparand2, nil, AArrayStartIndex);
end;

class function TSequence<T>.CompareWithPointer(const AComparand1: TSequence<T>;
  const AComparand2: Pointer): Boolean;
begin
  if AComparand2 <> nil then
    raise EInvalidOp.Create(SErrCannotCompareNotNullValueWithRecList);
  Result := AComparand1.Empty;
end;

class function TSequence<T>.CompareWithArray(const AComparand1: TSequence<T>;
  const AComparand2: TArray<T>; const AComparer: IEqualityComparer<T>;
  AArrayStartIndex: NativeInt): Boolean;
var
  LComparer: IEqualityComparer<T>;
  LItem: T;
  LIndex, LArrayLength: NativeInt;
begin
  LComparer := TFunctionalUtils.GetEqualityComparerOrDefault<T>(AComparer);
  LArrayLength := Length(AComparand2);
  LIndex := AArrayStartIndex;
  for LItem in AComparand1 do
  begin
    if (LIndex >= LArrayLength) or
      not LComparer.Equals(LItem, AComparand2[LIndex]) then
        Exit(False);
     Inc(LIndex);
  end;
  Result := LIndex = LArrayLength;
end;

class function TSequence<T>.Concatenate(const ASequence1,
  ASequence2: TSequence<T>): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    begin
      AProducer.YieldRecursive(ASequence1);
      AProducer.YieldRecursive(ASequence2);
    end
  );
end;

constructor TSequence<T>.Create(const AEnumerable: IEnumerable<T>);
begin
  FEnumerable := AEnumerable;
end;

class function TSequence<T>.CreateEmpty: TSequence<T>;
begin
  FillChar(Result, SizeOf(Result), 0);
end;

class operator TSequence<T>.Equal(const AComparand1: TSequence<T>;
  AComparand2: Pointer): Boolean;
begin
  Result := CompareWithPointer(AComparand1, AComparand2);
end;

class operator TSequence<T>.Equal(const AComparand1,
  AComparand2: TSequence<T>): Boolean;
begin
  Result := Compare(AComparand1, AComparand2);
end;

class operator TSequence<T>.Equal(const AComparand1: TSequence<T>;
  const AComparand2: TArray<T>): Boolean;
begin
  Result := CompareWithArray(AComparand1, AComparand2);
end;

function TSequence<T>.Filter(const APredicate: TPredicate<T>): TSequence<T>;
var
  LSequence: TSequence<T>;
begin
  LSequence := Self;
  Result := TSequence<T>.Create(
    procedure (const AProducer: TSequence<T>.IProducer)
    var
      LItem: T;
    begin
      for LItem in LSequence do
        if APredicate(LItem) then
          AProducer.Yield(LItem);
    end
  );
end;

function TSequence<T>.Fold<TResult>(const AFoldFunc: TFoldFunc<TResult>;
  const AAccum: TResult): TResult;
var
  LItem: T;
begin
  Result := AAccum;
  for LItem in Self do
    Result := AFoldFunc(Result, LItem);
end;

procedure TSequence<T>.ForAll(const AProc: TProc<T>);
var
  LItem: T;
begin
  for LItem in Self do
    AProc(LItem);
end;

class function TSequence<T>.FromArray(const AValue: TArray<T>): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    var
      LItem: T;
    begin
      for LItem in AValue do
        AProducer.Yield(LItem);
    end
  );
end;

class function TSequence<T>.FromRecList(
  const AValue: TRecList<T>): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    begin
      if AValue.Empty then
        Exit;
      AProducer.Yield(AValue.Head);
      AProducer.YieldRecursive(TSequence<T>.FromRecList(AValue.Tail));
    end
  );
end;

class function TSequence<T>.FromSingleItem(const AValue: T): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    begin
      AProducer.Yield(AValue);
    end
  );
end;

function TSequence<T>.GetEmpty: Boolean;
begin
  if not Assigned(FEnumerable) then
    Exit(True);
  Result := not GetEnumerator.MoveNext;
end;

function TSequence<T>.GetEnumerator: IEnumerator<T>;
begin
  if Assigned(FEnumerable) then
    Result := FEnumerable.GetEnumerator()
  else
    Result := TFunctionalUtils.GetEmptyEnumerator<T>();
end;

function TSequence<T>.GetSize: NativeInt;
var
  LItem: T;
begin
  Result := 0;
  for LItem in Self do
    Inc(Result);
end;

class operator TSequence<T>.GreaterThan(const ASequence: TSequence<T>;
  const AReduceFunc: TReduceFunc): T;
begin
  Result := ASequence.Reduce(AReduceFunc);
end;

class operator TSequence<T>.GreaterThanOrEqual(const ASequence: TSequence<T>;
  const APredicate: TPredicate<T>): TSequence<T>;
begin
  Result := ASequence.Filter(APredicate);
end;

class operator TSequence<T>.Implicit(AValue: Pointer): TSequence<T>;
begin
  if AValue <> nil then
    raise EInvalidOpException.Create(SErrCannotAssignNotNullValueToSequence);
  Result := CreateEmpty;
end;

class function TSequence<T>.InitInfinite(
  const AGeneratorFunc: TFunc<NativeInt, T>): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    var
      I: NativeInt;
    begin
      I := 0;
      while True do begin
        AProducer.Yield(AGeneratorFunc(I));
        Inc(I);
      end;  
    end
  );
end;

class operator TSequence<T>.LessThan(const ASequence: TSequence<T>;
  const AProc: TProc<T>): TSequence<T>;
begin
  ASequence.ForAll(AProc);
  Result := ASequence;
end;

class operator TSequence<T>.LessThan(const ASequence: TSequence<T>;
  const AMapFunc: TFunc<T, T>): TSequence<T>;
begin
  Result := ASequence.Map(AMapFunc);
end;

class operator TSequence<T>.Implicit(const AValue: TRecList<T>): TSequence<T>;
begin
  Result := FromRecList(AValue);
end;

class operator TSequence<T>.Implicit(const AValue: T): TSequence<T>;
begin
  Result := FromSingleItem(AValue);
end;

class operator TSequence<T>.Implicit(const AValue: TArray<T>): TSequence<T>;
begin
  Result := FromArray(AValue);
end;

class operator TSequence<T>.Implicit(
  const AValue: IEnumerable<T>): TSequence<T>;
begin
  Result := TSequence<T>.Create(AValue);
end;

class operator TSequence<T>.Implicit(
  const AValue: TGeneratorProc): TSequence<T>;
begin
  Result := TSequence<T>.Create(AValue);
end;

function TSequence<T>.Map(const AMapFunc: TFunc<T, T>): TSequence<T>;
begin
  Result := Map<T>(AMapFunc);
end;

function TSequence<T>.Map<TResult>(
  const AMapFunc: TFunc<T, TResult>): TSequence<TResult>;
var
  LSequence: TSequence<T>;
begin
  LSequence := Self;
  Result := TSequence<TResult>.Create(
    procedure (const AProducer: TSequence<TResult>.IProducer)
    var
      LItem: T;
    begin
      for LItem in LSequence do
        AProducer.Yield(AMapFunc(LItem));
    end
  );
end;

class operator TSequence<T>.NotEqual(const AComparand1: TSequence<T>;
  AComparand2: Pointer): Boolean;
begin
  Result := not CompareWithPointer(AComparand1, AComparand2);
end;

class operator TSequence<T>.NotEqual(const AComparand1: TSequence<T>;
  const AComparand2: TArray<T>): Boolean;
begin
  Result := not CompareWithArray(AComparand1, AComparand2);
end;

function TSequence<T>.Reduce(const AReduceFunc: TReduceFunc): T;
var
  LItem: T;
  LResultAssigned: Boolean;
begin
  if Empty then
    raise EInvalidOpException.Create(SErrReduceEmptySequence);
  LResultAssigned := False;
  for LItem in Self do
  begin
    if LResultAssigned then
      Result := AReduceFunc(Result, LItem)
    else begin
      Result := LItem;
      LResultAssigned := True;
    end;
  end;
  Assert(LResultAssigned);
end;

function TSequence<T>.Reduce(const AReduceFunc: TReduceFunc;
  const AAccum: T): T;
var
  LItem: T;
begin
  Result := AAccum;
  for LItem in Self do
    Result := AReduceFunc(Result, LItem);
end;

function TSequence<T>.Skip(ASkippedCount: NativeInt): TSequence<T>;
var
  LSelf: TSequence<T>;
begin
  if ASkippedCount <= 0 then
    Exit(Self);
  LSelf := Self;
  Result := TSequence<T>.Create(
    procedure (const AProducer: IProducer)
    var
      LItem: T;
      LCounter: NativeInt;
    begin
      LCounter := 0;
      for LItem in LSelf do begin
        if LCounter >= ASkippedCount then
          AProducer.Yield(LItem);
        Inc(LCounter);
      end;
    end
  )
end;

class operator TSequence<T>.NotEqual(const AComparand1,
  AComparand2: TSequence<T>): Boolean;
begin
  Result := not Compare(AComparand1, AComparand2);
end;

class function TSequence<T>.Unfold<TState>(const AUnfoldFunc: TUnfoldFunc<TState>; 
  const AInitialState: TState): TSequence<T>;
begin
  Result := TSequence<T>.Create(
    procedure (const AProducer: TSequence<T>.IProducer)
    var
      LState: TState;
      LIntermediateResult: TUnfoldFuncResult<TState>;
    begin
      LState := AInitialState;
      while True do
      begin
        LIntermediateResult := AUnfoldFunc(LState);
        if LIntermediateResult.Complete then
          Break;
        LState := LIntermediateResult.NewState;
        AProducer.Yield(LIntermediateResult.Item);
      end;
    end
  );
end;

{ TSequence<T>.TGenaratedEnumerator }

procedure TSequence<T>.TGenaratedEnumerator.BeforeDestruction;
begin
  inherited;
  FDestroying := True;
  if FGeneratorProcStarted then
    MoveNext;
end;

constructor TSequence<T>.TGenaratedEnumerator.Create(const AGeneratorProc: TGeneratorProc);
begin
  inherited Create;
  FGeneratorProc := AGeneratorProc;
  FFiber := TFiber.CreateProcFiber(
    procedure ()
    begin
      FiberProc();
    end
  );
end;

destructor TSequence<T>.TGenaratedEnumerator.Destroy;
begin
  FreeAndNil(FGeneratorProcExceptObject);
  FreeAndNil(FFiber);
  inherited;
end;

procedure TSequence<T>.TGenaratedEnumerator.FiberProc;
begin
  FGeneratorProcStarted := True;
  try
    try
      FGeneratorProc(Self);
    except
      if not (ExceptObject is EAbort) then
      begin
        FGeneratorProcExceptObject := AcquireExceptionObject;
        FGeneratorProcExceptAddr := ExceptAddr;
      end;
    end;
  finally
    FGeneratorProcTerminated := True;
    FLastItem := Default(T);
    SwitchToCallingFiber();
  end;
end;

function TSequence<T>.TGenaratedEnumerator.GetCurrent: T;
begin
  if not FGeneratorProcStarted then
    raise EFunctional.Create('Access to item below the range of sequence');
  if FGeneratorProcTerminated then
    raise EFunctional.Create('Access to item above the range of sequence');
  Result := FLastItem;
end;

function TSequence<T>.TGenaratedEnumerator.MoveNext: Boolean;
var
  CallingFiberIsThread: Boolean;
begin
  if FGeneratorProcTerminated then
    Exit(False);
  FCallingThreadFiber := TFiber.CreateThreadFiber();
  try
    FFiber.SwitchTo();
    if Assigned(FGeneratorProcExceptObject) then
      ReraiseGeneratorProcException();
    Result := not FGeneratorProcTerminated;
  finally
    FreeAndNil(FCallingThreadFiber);
  end;
end;

procedure TSequence<T>.TGenaratedEnumerator.ReraiseGeneratorProcException;
var
  LExceptObject: TObject;
  LExceptAddr: Pointer;
begin
  LExceptObject := FGeneratorProcExceptObject;
  LExceptAddr := FGeneratorProcExceptAddr;
  FGeneratorProcExceptObject := nil;
  FGeneratorProcExceptAddr := nil;
  raise LExceptObject at LExceptAddr;
end;

procedure TSequence<T>.TGenaratedEnumerator.SwitchToCallingFiber();
begin
  TFiber.SwitchToFiber(FCallingThreadFiber);
end;

procedure TSequence<T>.TGenaratedEnumerator.Yield(const AItem: T);
begin
  FLastItem := AItem;
  SwitchToCallingFiber();
  if FDestroying then
    Abort;
end;

procedure TSequence<T>.TGenaratedEnumerator.YieldRecursive(
  const AItems: TSequence<T>);
var
  Item: T;
begin
  for Item in AItems do
    Yield(Item);
end;

{ TSequence<T>.TUnfoldFuncResult<TState> }

class function TSequence<T>.TUnfoldFuncResult<TState>.CreateItem(const AItem: T;
  const ANewState: TState): TUnfoldFuncResult<TState>;
begin
  FillChar(Result, Sizeof(Result), 0);
  Result.FItem := AItem;
  Result.FNewState := ANewState;
end;

class function TSequence<T>.TUnfoldFuncResult<TState>.CreteComplete: TUnfoldFuncResult<TState>;
begin
  FillChar(Result, Sizeof(Result), 0);
  Result.FComplete := True;
end;

end.

