unit Functional.Fibers;

interface

uses
  Winapi.Windows, System.SysUtils,
  Functional.Winapi.Fibers;

type
  TFiber = class abstract
  strict private
    FHandle: Pointer;
    class function GetCurrent(): TFiber; static;
  strict protected
    constructor Create(AHandle: Pointer);
    property Handle: Pointer read FHandle;
  public
    procedure SwitchTo();
    class procedure SwitchToFiber(AFiber: TFiber); overload;
    class procedure SwitchToFiber(AFiberHandle: Pointer); overload;

    class function CreateFiber(AHandle: Pointer): TFiber;
    class function CreateProcFiber(const AFiberProc: TProc;
      AStackSize: NativeUInt = 0): TFiber;
    class function CreateThreadFiber(): TFiber;

    class function IsThreadAFiber(): Boolean;
    class property Current: TFiber read GetCurrent;
  end;

implementation

const
  IsThreadAFiberRequiredOSMajorVersion = 6;

type
  TProcFiber = class(TFiber)
  strict private
    FProc: TProc;
    class procedure GlobalFiberProc(lpParameter: Pointer); static; stdcall;
    procedure FiberProc();
  public
    constructor Create(const AFiberProc: TProc; AStackSize: NativeUInt = 0);
    destructor Destroy(); override;
  end;

  TThreadFiber = class(TFiber)
  strict private
    FInheritedFiber: Boolean;
  public
    constructor Create();
    destructor Destroy(); override;
  end;

{ TFiber }

constructor TFiber.Create(AHandle: Pointer);
begin
  inherited Create();
  FHandle := AHandle;
end;

class function TFiber.CreateFiber(AHandle: Pointer): TFiber;
begin
  Result := TFiber.Create(AHandle);
end;

class function TFiber.CreateProcFiber(const AFiberProc: TProc;
  AStackSize: NativeUInt): TFiber;
begin
  Result := TProcFiber.Create(AFiberProc, AStackSize);
end;

class function TFiber.CreateThreadFiber(): TFiber;
begin
  Result := TThreadFiber.Create();
end;

class function TFiber.GetCurrent(): TFiber;
begin
  Result := TFiber(GetFiberData());
end;

class function TFiber.IsThreadAFiber: Boolean;
begin
  // TODO: workaraound for older windows version
  if not TOSVersion.Check(IsThreadAFiberRequiredOSMajorVersion) then
    raise EInvalidOpException.Create('Unsupported OS version');
  Result := Functional.Winapi.Fibers.IsThreadAFiber();
end;

procedure TFiber.SwitchTo();
begin
  TFiber.SwitchToFiber(FHandle);
end;

class procedure TFiber.SwitchToFiber(AFiber: TFiber);
begin
  TFiber.SwitchToFiber(AFiber.Handle);
end;

class procedure TFiber.SwitchToFiber(AFiberHandle: Pointer);
begin
  Winapi.Windows.SwitchToFiber(AFiberHandle);
end;

{ TProcFiber }

constructor TProcFiber.Create(const AFiberProc: TProc; AStackSize: NativeUInt);
var
  LHandle: Pointer;
begin
  LHandle := Winapi.Windows.CreateFiber(AStackSize, @GlobalFiberProc, Pointer(Self));
  if LHandle = nil then
    RaiseLastOSError();
  FProc := AFiberProc;
  inherited Create(LHandle);
end;

destructor TProcFiber.Destroy;
begin
  if Handle <> nil then
    DeleteFiber(Handle);
  inherited;
end;

procedure TProcFiber.FiberProc;
begin
  FProc();
end;

class procedure TProcFiber.GlobalFiberProc(lpParameter: Pointer);
begin
  TProcFiber(lpParameter).FiberProc();
end;

{ TThreadFiber }

constructor TThreadFiber.Create;
var
  LHandle: Pointer;
begin
  LHandle := Functional.Winapi.Fibers.ConvertThreadToFiber(Pointer(Self));
  if (LHandle = nil) and (GetLastError = ERROR_ALREADY_FIBER) then begin
    LHandle := GetCurrentFiber;
    FInheritedFiber := True;
  end;
  if LHandle = nil then
    RaiseLastOSError;
  inherited Create(LHandle);
end;

destructor TThreadFiber.Destroy;
begin
  if not FInheritedFiber then
    ConvertFiberToThread();
  inherited;
end;

end.
