unit Functional.Winapi.Fibers;

interface

uses
  Windows;

function ConvertThreadToFiber(lpParameter: Pointer): Pointer; stdcall;
function ConvertFiberToThread: BOOL; stdcall;
function IsThreadAFiber: BOOL; stdcall;

function GetCurrentFiber: Pointer;
function GetFiberData: Pointer; inline;

//
// MessageId: ERROR_ALREADY_FIBER
//
// MessageText:
//
// The current thread has already been converted to a fiber.
//
const
  ERROR_ALREADY_FIBER = 1280;

implementation

function ConvertThreadToFiber; external kernel32;
function ConvertFiberToThread; external kernel32;
{$WARN SYMBOL_PLATFORM OFF}
function IsThreadAFiber; external kernel32 delayed;
{$WARN SYMBOL_PLATFORM DEFAULT}

function GetCurrentFiber: Pointer;
asm
{$IFDEF CPUX64}
//  MOV RAX, GS[$20] // BASM64 generates wrong code => $20 becomes image00000000_00400000+0x25af9
  DB $65, $48, $8B, $04, $25, $20, $00, $00, $00
{$ELSE}
  MOV EAX, FS:[$10]
{$ENDIF}
end;

function GetFiberData: Pointer;
begin
  Result := PPointer(GetCurrentFiber()^);
end;

end.
