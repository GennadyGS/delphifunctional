unit Functional.Utils;

interface

uses
  SysUtils, Classes, Rtti, Variants, Generics.Defaults, Generics.Collections;

type
  EFunctional = class(Exception);
  EEnumerator = class(EFunctional);

  IWrapper<T> = interface
    function GetValue(): T;
    property Value: T read GetValue;
  end;

  TWrapper<T> = class(TInterfacedObject, IWrapper<T>)
  strict private
    FValue: T;
  strict protected
    { IWrapper<T> }
    function GetValue(): T;
  public
    constructor Create(const AValue: T);
  end;

  IEnumerator<T> = interface
    function GetCurrent(): T;
    function MoveNext(): Boolean;
    property Current: T read GetCurrent;
  end;

  IEnumerable<T> = interface
    function GetEnumerator(): IEnumerator<T>;
  end;

  TEmptyEnumerator<T> = class(TInterfacedObject, IEnumerator<T>)
  strict protected
    { IEnumerator<T> }
    function GetCurrent(): T;
    function MoveNext(): Boolean;
  end;

  TEnumeratorFactory<T> = class(TInterfacedObject, IEnumerable<T>)
  strict private
    FFactoryFunc: TFunc<IEnumerator<T>>;
  strict protected
    { IEnumerable<T> }
    function GetEnumerator(): IEnumerator<T>;
  public
    constructor Create(const AFactoryFunc: TFunc<IEnumerator<T>>);
  end;

  TFunctionalUtils = class
    class function GetEmptyEnumerator<T>(): IEnumerator<T>;
    class function CreateEnumeratorFactory<T>(const AFactoryFunc: TFunc<IEnumerator<T>>): IEnumerable<T>;

    class function GetEqualityComparerOrDefault<T>(const AComparer: IEqualityComparer<T>): IEqualityComparer<T>;
  end;

implementation

{ TWrapper<T> }

constructor TWrapper<T>.Create(const AValue: T);
begin
  inherited Create;
  FValue := AValue;
end;

function TWrapper<T>.GetValue(): T;
begin
  Result := FValue;
end;

{ TEnumeratorFactory<T> }

constructor TEnumeratorFactory<T>.Create(
  const AFactoryFunc: TFunc<IEnumerator<T>>);
begin
  inherited Create;
  FFactoryFunc := AFactoryFunc;
end;

function TEnumeratorFactory<T>.GetEnumerator(): IEnumerator<T>;
begin
  Result := FFactoryFunc();
end;

{ TFunctionalUtils }

class function TFunctionalUtils.CreateEnumeratorFactory<T>(
  const AFactoryFunc: TFunc<IEnumerator<T>>): IEnumerable<T>;
begin
  Result := TEnumeratorFactory<T>.Create(AFactoryFunc);
end;

class function TFunctionalUtils.GetEmptyEnumerator<T>(): IEnumerator<T>;
begin
  Result := TEmptyEnumerator<T>.Create();
end;

class function TFunctionalUtils.GetEqualityComparerOrDefault<T>(
  const AComparer: IEqualityComparer<T>): IEqualityComparer<T>;
begin
  if not Assigned(AComparer) then
    Exit(TEqualityComparer<T>.Default);
  Result := AComparer;
end;

{ TEmptyEnumerator<T> }

function TEmptyEnumerator<T>.GetCurrent(): T;
begin
  raise EEnumerator.Create('Enumerator is empty');
end;

function TEmptyEnumerator<T>.MoveNext(): Boolean;
begin
  Result := False;
end;

end.

