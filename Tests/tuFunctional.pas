unit tuFunctional;

interface

uses
  Generics.Collections,
  TestFramework,
  Functional.Utils, Functional.Base;

type
  TTestRecList = class(TTestCase)
  strict private
    FList0: TRecList<NativeInt>;
    FList1: TRecList<NativeInt>;
    FList2: TRecList<NativeInt>;
    FList3: TRecList<NativeInt>;
  protected
    procedure SetUp; override;
  published
    procedure TestAccess;
    procedure TestCompare;
    procedure TestConcatenate;
    procedure TestGetSize;
    procedure TestReverse;
    procedure TestMap;
    procedure TestFold;
    procedure TestForAll;
    procedure TestReduceWithAccum;
    procedure TestReduce;
    procedure TestReduceEmpty;
  end;

  TTestSequence = class(TTestCase)
  strict private
    FSequence0: TSequence<NativeInt>;
    FSequence1: TSequence<NativeInt>;
    FSequence2: TSequence<NativeInt>;
    FSequence3: TSequence<NativeInt>;
    FSequenceNested: TSequence<NativeInt>;
    FSequenceInfinite: TSequence<NativeInt>;
    FSequenceWithException: TSequence<NativeInt>;
    FSequenceRec: TSequence<NativeInt>;
    procedure CheckNaturalSequence(const ASequence: TSequence<NativeInt>;
      AMaxCount: NativeInt = -1);
  protected
    procedure SetUp; override;
  published
    procedure TestEnum;
    procedure TestEmpty;
    procedure TestGetSize;
    procedure TestCompare;
    procedure TestFinally;
    procedure TestFromArray;
    procedure TestFromSingleItem;
    procedure TestFromRecList;
    procedure TestInitInfinite;
    procedure TestMap;
    procedure TestFilter;
    procedure TestFold;
    procedure TestForAll;
    procedure TestReduceWithAccum;
    procedure TestReduce;
    procedure TestReduceEmpty;
    procedure TestSkip;
    procedure TestUnfold;
  end;

implementation

uses
  System.SysUtils;

function CheckConcatStr(const ALeft, ARight, ADelimiter: String): String;
begin
  if ARight = '' then
    Exit(ALeft);
  if ALeft = '' then
    Exit(ARight);
  Result := ALeft + ADelimiter + ARight;
end;

{ TTestRecList }

procedure TTestRecList.SetUp;
begin
  inherited;
  FList0 := nil;
  FList1 := 1;
  FList2 := 2 * FList1;
  FList3 := 3 * FList2;
end;

procedure TTestRecList.TestAccess;
begin
  CheckTrue(FList0.Empty);
  CheckEquals(FList1.Head, 1);
  CheckTrue(FList1.Tail.Empty);
  CheckEquals(FList2.Head, 2);
  CheckFalse(FList2.Tail.Empty);
  CheckEquals(FList2.Tail.Head, 1);
  CheckTrue(FList2.Tail.Tail.Empty);
  CheckEquals(FList3.Head, 3);
  CheckFalse(FList3.Tail.Empty);
  CheckEquals(FList3.Tail.Head, 2);
  CheckFalse(FList3.Tail.Tail.Empty);
  CheckEquals(FList3.Tail.Tail.Head, 1);
end;

procedure TTestRecList.TestCompare;
begin
  CheckTrue(FList0 = nil);
  CheckFalse(FList0 <> nil);
  CheckTrue(FList1 <> nil);
  CheckTrue(FList1 = TArray<NativeInt>.Create(1));
  CheckTrue(FList1 <> TArray<NativeInt>.Create(1, 2));
  CheckTrue(FList2 <> nil);
  CheckTrue(FList2 <> TArray<NativeInt>.Create(1));
  CheckTrue(FList2 = TArray<NativeInt>.Create(2, 1));
  CheckTrue(FList2 <> TArray<NativeInt>.Create(3, 2, 1));
  CheckTrue(FList3 <> nil);
  CheckTrue(FList3 <> TArray<NativeInt>.Create(1));
  CheckTrue(FList3 <> TArray<NativeInt>.Create(2, 1));
  CheckTrue(FList3 = TArray<NativeInt>.Create(3, 2, 1));
  CheckTrue(FList3 <> TArray<NativeInt>.Create(3, 2, 1, 0));
  CheckTrue(FList0 = FList0);
  CheckTrue(FList1 = FList1);
  CheckTrue(FList2 = FList2);
  CheckTrue(FList3 = FList3);
end;

procedure TTestRecList.TestConcatenate;
begin
  CheckTrue(FList3 + FList0 = FList3);
  CheckTrue(FList3 + FList3 = TArray<NativeInt>.Create(3, 2, 1, 3, 2, 1));
  CheckTrue(FList0 + nil = nil);
end;

procedure TTestRecList.TestFold;
var
  Res: String;
begin
  Res := FList0.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '');
  Res := FList1.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '1');
  Res := FList3.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '3,2,1');
end;

procedure TTestRecList.TestForAll;
var
  Res: NativeInt;
begin
  Res := 0;
  FList3.ForAll(
    procedure (AItem: NativeInt)
    begin
      Res := Res + AItem;
    end
  );
  CheckEquals(Res, 3 + 2 + 1);
end;

procedure TTestRecList.TestGetSize;
begin
  CheckEquals(FList0.Size, 0);
  CheckEquals(FList1.Size, 1);
  CheckEquals(FList2.Size, 2);
  CheckEquals(FList3.Size, 3);
end;

procedure TTestRecList.TestMap;
var
  ResList: TRecList<String>;
begin
  ResList := FList0.Map<String>(
    function (AItem: NativeInt): String
    begin
      Result := IntToStr(AItem);
    end
  );
  CheckTrue(ResList = nil);
  ResList := FList3.Map<String>(
    function (AItem: NativeInt): String
    begin
      Result := IntToStr(AItem);
    end
  );
  CheckTrue(ResList = TArray<String>.Create('3', '2', '1'));
end;

procedure TTestRecList.TestReduceWithAccum;
var
  Res: NativeInt;
begin
  Res := FList0.Reduce(
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end,
    0);
  CheckEquals(Res, 0);
  Res := FList3.Reduce(
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end,
    0
  );
  CheckEquals(Res, 3 + 2 + 1);
end;

procedure TTestRecList.TestReduce;
var
  Res: NativeInt;
begin
  Res := FList3 >
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end;
  CheckEquals(Res, 3 + 2 + 1);
end;

procedure TTestRecList.TestReduceEmpty;
begin
  ExpectedException := EInvalidOpException;
  FList0 >
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end;
end;

procedure TTestRecList.TestReverse;
begin
  CheckTrue(FList0.Reverse = nil);
  CheckTrue(FList3.Reverse = TArray<NativeInt>.Create(1, 2, 3));
end;

{ TTestSequence }

procedure TTestSequence.SetUp;
begin
  inherited;
  FSequence1 :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      AProducer.Yield(1);
    end;
  FSequence2 :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      AProducer.Yield(1);
      AProducer.Yield(2);
    end;
  FSequence3 :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      AProducer.Yield(1);
      AProducer.Yield(2);
      AProducer.Yield(3);
    end;
  FSequenceNested :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      AProducer.Yield(1);
      AProducer.Yield(2);
      AProducer.YieldRecursive(
        procedure (const AProducer: TSequence<NativeInt>.IProducer)
        begin
          AProducer.Yield(3);
          AProducer.Yield(4);
        end
      );
    end;
  FSequenceInfinite :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    var
      I: Integer;
    begin
      I := 1;
      while True do
      begin
        AProducer.Yield(I);
        Inc(I);
      end;
    end;
  FSequenceRec :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    var
      I: Integer;
    begin
      for I := 0 to 3 do
        AProducer.YieldRecursive(TSequence<NativeInt>.Create(
          procedure (const AProducer: TSequence<NativeInt>.IProducer)
          var
            J: Integer;
          begin
            for J := 0 to I - 1 do
              AProducer.Yield(I);
          end
        ))
    end;
  FSequenceWithException :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      AProducer.Yield(1);
      AProducer.Yield(2);
      raise EFunctional.Create('Text exception');
    end
end;

procedure TTestSequence.TestCompare;
begin
  CheckTrue(FSequence0 = nil);
  CheckFalse(FSequence0 <> nil);
  CheckTrue(FSequence1 <> nil);
  CheckTrue(FSequence1 = TArray<NativeInt>.Create(1));
  CheckTrue(FSequence1 <> TArray<NativeInt>.Create(1, 2));
  CheckTrue(FSequence2 <> nil);
  CheckTrue(FSequence2 <> TArray<NativeInt>.Create(1));
  CheckTrue(FSequence2 = TArray<NativeInt>.Create(1, 2));
  CheckTrue(FSequence2 <> TArray<NativeInt>.Create(1, 2, 3));
  CheckTrue(FSequence3 <> nil);
  CheckTrue(FSequence3 <> TArray<NativeInt>.Create(1));
  CheckTrue(FSequence3 <> TArray<NativeInt>.Create(1, 2));
  CheckTrue(FSequence3 = TArray<NativeInt>.Create(1, 2, 3));
  CheckTrue(FSequence3 <> TArray<NativeInt>.Create(1, 2, 3, 4));
  CheckTrue(FSequence0 = FSequence0);
  CheckTrue(FSequence1 = FSequence1);
  CheckTrue(FSequence2 = FSequence2);
  CheckTrue(FSequence3 = FSequence3);
end;

procedure TTestSequence.TestEmpty;
begin
  CheckTrue(FSequence0.Empty);
  CheckFalse(FSequence1.Empty);
  CheckFalse(FSequence2.Empty);
  CheckFalse(FSequence3.Empty);
  CheckFalse(FSequenceNested.Empty);
  CheckTrue(TSequence<NativeInt>.CreateEmpty.Empty);
end;

procedure TTestSequence.TestEnum;
var
  I: NativeInt;
begin
  for I in FSequence0 do
    CheckTrue(I <> I); // Should be never called
  CheckNaturalSequence(FSequence2);
  CheckNaturalSequence(FSequence3);
  CheckNaturalSequence(FSequenceInfinite, 1000);
  CheckNaturalSequence(FSequenceNested);
  ExpectedException := EFunctional;
  CheckNaturalSequence(FSequenceWithException);
end;

procedure TTestSequence.TestFilter;
var
  Seq: TSequence<NativeInt>;
begin
  Seq := FSequence3 >=
    function (AItem: NativeInt): Boolean
    begin
      Result := Odd(AItem);
    end;
  CheckTrue(Seq = Tarray<NativeInt>.Create(1, 3));
end;

procedure TTestSequence.TestFinally;
var
  I, L: NativeInt;
  Seq: TSequence<NativeInt>;
begin
  L := 0;
  Seq :=
    procedure (const AProducer: TSequence<NativeInt>.IProducer)
    begin
      try
        AProducer.Yield(1);
        AProducer.Yield(2);
      finally
        L := 1;
      end;
    end;
  for I in Seq do
  begin
    CheckEquals(I, 1);
    Break;
  end;
  CheckEquals(L, 1);
end;

procedure TTestSequence.TestFold;
var
  Res: String;
begin
  Res := FSequence0.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '');
  Res := FSequence1.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '1');
  Res := FSequence3.Fold<String>(
    function (const AAccum: String; const AItem: NativeInt): String
    begin
      Result := CheckConcatStr(AAccum, IntToStr(AItem), ',');
    end,
    '');
  CheckEquals(Res, '1,2,3');
end;

procedure TTestSequence.TestForAll;
var
  Res: NativeInt;
begin
  Res := 0;
  FSequence3.ForAll(
    procedure (AItem: NativeInt)
    begin
      Res := Res + AItem;
    end
  );
  CheckEquals(Res, 1 + 2 + 3);
end;

procedure TTestSequence.TestFromArray;
var
  LArray: TArray<NativeInt>;
  LSequence: TSequence<NativeInt>;
begin
  LArray := TArray<NativeInt>.Create(1, 2, 3);
  LSequence := LArray;
  CheckEquals(LSequence.Size, Length(LArray));
  CheckNaturalSequence(LSequence);
end;

procedure TTestSequence.TestFromRecList;
var
  LArray: TArray<NativeInt>;
  LRecList: TRecList<NativeInt>;
  LSequence: TSequence<NativeInt>;
begin
  LArray := TArray<NativeInt>.Create(1, 2, 3);
  LRecList := LArray;
  LSequence := LRecList;
  CheckEquals(LSequence.Size, Length(LArray));
  CheckNaturalSequence(LSequence);
end;

procedure TTestSequence.TestFromSingleItem;
var
  LSequence: TSequence<NativeInt>;
begin
  LSequence := 1;
  CheckNaturalSequence(LSequence);
  CheckEquals(LSequence.Size, 1);
end;

procedure TTestSequence.TestGetSize;
begin
  CheckEquals(FSequence0.Size, 0);
  CheckEquals(FSequence1.Size, 1);
  CheckEquals(FSequence2.Size, 2);
  CheckEquals(FSequence3.Size, 3);
  CheckEquals(FSequenceNested.Size, 4);
  // CheckEquals(FSequenceInfinite.Size, 0); // Infinite loop
end;

procedure TTestSequence.TestInitInfinite;
var
  LSequence: TSequence<NativeInt>;
begin
  LSequence := TSequence<NativeInt>.InitInfinite(
    function (AIndex: NativeInt): NativeInt
    begin
      Result := AIndex + 1;
    end
  );
  CheckNaturalSequence(LSequence, 1000);
end;

procedure TTestSequence.TestMap;
var
  Sequence: TSequence<String>;
begin
  Sequence := FSequence0.Map<String>(
    function (AItem: NativeInt): String
    begin
      Result := IntToStr(AItem);
    end
  );
  CheckTrue(Sequence = nil);
  Sequence := FSequence3.Map<String>(
    function (AItem: NativeInt): String
    begin
      Result := IntToStr(AItem);
    end
  );
  CheckTrue(Sequence = TArray<String>.Create('1', '2', '3'));
end;

procedure TTestSequence.TestReduce;
var
  Res: NativeInt;
begin
  Res := FSequence3 >
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end;
  CheckEquals(Res, 1 + 2 + 3);
end;

procedure TTestSequence.TestReduceEmpty;
begin
  ExpectedException := EInvalidOpException;
  FSequence0 >
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end;
end;

procedure TTestSequence.TestReduceWithAccum;
var
  Res: NativeInt;
begin
  Res := FSequence0.Reduce(
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end,
    0);
  CheckEquals(Res, 0);
  Res := FSequence3.Reduce(
    function (const AAccum, AItem: NativeInt): NativeInt
    begin
      Result := AAccum + AItem;
    end,
    0
  );
  CheckEquals(Res, 1 + 2 + 3);
end;

procedure TTestSequence.TestSkip;
var
  Seq: TSequence<NativeInt>;
begin
  Seq := FSequence0.Skip(1);
  CheckTrue(Seq = nil);
  Seq := FSequence1.Skip(0);
  CheckTrue(FSequence1 = Seq);
  Seq := FSequence1.Skip(2);
  CheckTrue(Seq = nil);
  Seq := FSequence3.Skip(1);
  CheckTrue(Seq = TArray<NativeInt>.Create(2, 3));
end;

procedure TTestSequence.TestUnfold;
const
  CSeqLength = 100;
var
  Seq: TSequence<NativeInt>;
begin
  Seq := TSequence<NativeInt>.Unfold<NativeInt>(
    function (const AState: NativeInt): TSequence<NativeInt>.TUnfoldFuncResult<NativeInt>
    begin
      if AState >= CSeqLength then
        Exit(TSequence<NativeInt>.TUnfoldFuncResult<NativeInt>.CreteComplete);
      Result := TSequence<NativeInt>.TUnfoldFuncResult<NativeInt>.CreateItem(
        AState + 1, AState + 1);
    end,
    0
  );
  CheckNaturalSequence(Seq);
  CheckEquals(Seq.Size, CSeqLength);
end;

procedure TTestSequence.CheckNaturalSequence(const ASequence: TSequence<NativeInt>;
  AMaxCount: NativeInt = -1);
var
  I, Counter: NativeInt;
begin
  Counter := 1;
  for I in ASequence do
  begin
    CheckEquals(I, Counter);
    Inc(Counter);
    if (AMaxCount >= 0) and (Counter >= AMaxCount) then
      Break;
  end;
end;

initialization
  RegisterTest(TTestRecList.Suite);
  RegisterTest(TTestSequence.Suite);

end.
